# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/member-buymileage-confirm.jar /member-buymileage-confirm.jar
# run application with this command line[
CMD ["java", "-jar", "/member-buymileage-confirm.jar"]
